myApp.controller('ForumController', function($rootScope, $scope, $location, $state, $stateParams, $http) {
	$scope.selectedBranch = undefined;
	$scope.selectedSubject = undefined;
	$scope.newSubject = undefined;
	$scope.newMessage = undefined;

	$scope.$watchCollection(function(){
		return $state.params;
	}, function(){
		if($stateParams.forum_name != undefined && $stateParams.forum_name != '') {
			initSelectedBranch($stateParams.forum_name);
		} else {
			$scope.selectedBranch = undefined;
		}
	});

	$scope.$on('branchesLoaded', function(data) {
		console.log($stateParams);
		if($stateParams.forum_name != undefined && $stateParams.forum_name != '') {
			initSelectedBranch($stateParams.forum_name);
		} else {
			$scope.selectedBranch = undefined;
		}
	})

	var initSelectedBranch = function(name) {
		if($scope.branches != undefined) {
			var branchFounded = false;

			for(var i=0; i<$scope.branches.length; i++) {
				if($scope.branches[i].name == name) {
					$scope.selectedBranch = $scope.branches[i];
					branchFounded = true;
					break;
				}
			}

			if(!branchFounded) {
				$scope.selectedBranch = undefined;
				$location.path('forum');
			} else {
				// init subject if exists
				if($stateParams.subject_id != undefined &&
						$stateParams.subject_id != '' &&
						parseInt($stateParams.subject_id)) {
					var subjectFounded = false;

					for(var i=0; i<$scope.selectedBranch.subjects.length; i++ ) {
						if($scope.selectedBranch.subjects[i].id == $stateParams.subject_id) {
							$scope.selectedSubject = $scope.selectedBranch.subjects[i];
							subjectFounded = true;
							break;
						}
					}

					if(!subjectFounded) {
						$scope.selectedSubject = undefined;
						$location.path('forum/' + $scope.selectedBranch.name + '/subiecte');
					}
				} else {
					$scope.selectedSubject = undefined;
				}
			}
		}
	}

	$scope.setSelectedBranch = function(branch) {
		$scope.selectedBranch = branch;
		$location.path('/forum/' + branch.name + '/subiecte');
	}

	$scope.setSelectedSubject = function(subject) {
		$scope.selectedSubject = subject;
		$location.path('/forum/' + $scope.selectedBranch.name + '/subiecte/' + subject.id);
	}

	$scope.cancelNewSubject = function() {
		$scope.addSubject = false;
	}

	$scope.addNewSubject = function(form) {
		console.log($scope.newSubject);

		$http({
            method: 'POST',
            url: 'api/add-subject',
            data: {
            	branch_id: $scope.selectedBranch.id,
            	new_subject: $scope.newSubject
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.selectedBranch.subjects.push(data.subject);

            $scope.newSubject = undefined;

            form.$setPristine();
            form.$setUntouched();
            form.$dirty = false;
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}

	$scope.addNewMessage = function(form) {
		$http({
            method: 'POST',
            url: 'api/subject-add-message',
            data: {
            	subject_id: $scope.selectedSubject.id,
            	new_message: $scope.newMessage
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.selectedSubject.messages.push(data.message);

            $scope.newMessage = undefined;

            form.$setPristine();
            form.$setUntouched();
            form.$dirty = false;
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}

	$scope.deleteSubject = function(subject) {
		$http({
            method: 'POST',
            url: 'api/delete-subject',
            data: {
            	subject_id: subject.id,
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.selectedBranch.subjects.splice($scope.selectedBranch.subjects.indexOf(subject), 1);

            if($scope.selectedSubject != undefined) {
            	if($scope.selectedSubject.id == subject.id) {
	            	$scope.selectedSubject = undefined;
	            	$location.path('forum/' + $scope.selectedBranch.name + '/subiecte');
	            }
            }
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}

	$scope.deleteMessage = function(message) {
		$http({
            method: 'POST',
            url: 'api/delete-message',
            data: {
            	message_id: message.id,
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.selectedSubject.messages.splice($scope.selectedSubject.messages.indexOf(message), 1);
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}
})