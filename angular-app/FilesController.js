myApp.controller('FilesController', function($scope, $http, FilesService) {
	$scope.tab = 0;
	$scope.files = undefined;
	$scope.branch = undefined;

	$scope.$on('branchesLoaded', function(data) {
		$scope.branch = $scope.branches[0];
	})

	// load files from server
	var promise = FilesService.getFiles();
	promise.then(function (data) {
		console.log(data);
		$scope.files = data.data.files;
	});

	$scope.deleteFile = function(file) {
		$http({
	            method: 'POST',
	            url: 'api/delete-file',
	            data: {
	            	id: file.id
	            }
	        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.files.splice($scope.files.indexOf(file), 1);
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}
})