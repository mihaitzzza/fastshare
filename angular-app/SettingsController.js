myApp.controller('SettingsController', function($rootScope, $scope, $http, fileReader) {
	var initEditModel = function() {
		$scope.editUserModel = {
			id: $rootScope.authUser.id,
			firstname: $rootScope.authUser.firstname,
			lastname: $rootScope.authUser.lastname,
			email: $rootScope.authUser.email,
			initialAvatar: $rootScope.authUser.avatar,
			old_password: '',
			new_password: '',
			new_password_confirmation: '',
			myCroppedImage: '',
			iconAccepted: true,
			avatarSrc: '',
			uploadedAvatar: ''
		}

		$scope.errors = {
			email: '',
			password: ''
		}
	}

	initEditModel();

	$scope.triggerUpload = function() {
		var fileuploader = angular.element("#upload-avatar");

		fileuploader.on('click', function() {
		});

		fileuploader.trigger('click')
	};

	var dataURItoBlob = function(dataURI) {
		var binary = atob(dataURI.split(',')[1]);
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
		var array = [];
		for(var i = 0; i < binary.length; i++) {
			array.push(binary.charCodeAt(i));
		}
		return new Blob([new Uint8Array(array)], {type: mimeString});
	};

	/* Public methods */
	$scope.setFile = function(file) {
		$scope.editUserModel.iconAccepted = false;
		$scope.editUserModel.uploadedAvatar = file;
		fileReader.readAsDataUrl($scope.editUserModel.uploadedAvatar, $scope)
			.then(function(result) {
				$scope.editUserModel.avatarSrc = result;
			});
	};

	$scope.createDragAndDropZone = function() {
		if(window.File && window.FileReader && window.FileList && window.Blob) {
			function handleFileSelect(evt) {
				evt.stopPropagation();
				evt.preventDefault();

				var files = evt.dataTransfer.files; // FileList object.

				$scope.uploadedAvatar = files[0];
				$scope.setFile($scope.uploadedAvatar);

				$('#upload-avatar').val('');
			}

			function handleDragOver(evt) {
				evt.stopPropagation();
				evt.preventDefault();
			}

			// Setup the dnd listeners.
			var dropZone = document.getElementById('avatar-drag-and-drop-container');
			dropZone.addEventListener('dragover', handleDragOver, false);
			dropZone.addEventListener('drop', handleFileSelect, false);
		} else {
			window.alert('The File APIs are not fully supported in this browser.');
		}
	};

	$scope.saveSettings = function(form) {
		var formData = new FormData();

		for(key in $scope.editUserModel) {
			if(key != 'initialAvatar' && key != 'avatarSrc') {
				if(key != 'uploadedAvatar') {
					if(key == 'myCroppedImage' && $scope.editUserModel.myCroppedImage != '') {
						var myBlob = dataURItoBlob($scope.editUserModel.myCroppedImage);
						var originalNameArr = $scope.editUserModel.uploadedAvatar.name.split('.');
						var originalName = '';
						for(var i=0; i<originalNameArr.length; i++) {
							if(i < originalNameArr.length-1) {
								originalName += originalNameArr[i] + '.';
							}
						}
						originalName += 'png';

						formData.append('file', myBlob, originalName);
					} else {
						formData.append(key, $scope.editUserModel[key]);
					}
				}
			}
		}

		if($scope.editUserModel.uploadedAvatar != '') {
			var myBlob = dataURItoBlob($scope.editUserModel.myCroppedImage);
			var originalNameArr = $scope.editUserModel.uploadedAvatar.name.split('.');
			var originalName = '';
			for(var i=0; i<originalNameArr.length; i++) {
				if(i < originalNameArr.length-1) {
					originalName += originalNameArr[i] + '.';
				}
			}
			originalName += 'png';

			formData.append('file', myBlob, originalName);
		}

		$http({
			headers: {'Content-Type': undefined},
			method: 'POST',
			url: 'api/save-settings',
			data: formData
		})
		.success(function (data, status, headers, config) {
			console.log('EDIT PROFILE - Success', data);

			$rootScope.authUser = data.authUser;
			localStorage.setItem('authUser', JSON.stringify($rootScope.authUser));

			form.$setPristine();
			form.$setUntouched();
			form.$dirty = false;

			initEditModel();
		})
		.error(function (data, status, headers, config) {
			console.log('EDIT PROFILE - Error', data);

			if(status == 400) {
				if(data.errors) {
					if(data.errors.email) {
						$scope.errors.email = data.errors.email[0];
					}

					if(data.errors.old_password) {
						$scope.errors.password = data.errors.old_password[0];
					}
				}
			}
		});
	};
	
	$scope.acceptProfileIcon = function(form) {
		$('#upload-avatar').val('');

		$scope.editUserModel.iconAccepted = true;
		$scope.editUserModel.initialAvatar = $scope.editUserModel.myCroppedImage;

		form.$dirty = true;
	};

	$scope.cancelProfileIcon = function() {
		$('#upload-avatar').val('');

		$scope.editUserModel.myCroppedImage = '';
		$scope.editUserModel.uploadedAvatar = '';
		$scope.editUserModel.avatarSrc = '';
		$scope.editUserModel.iconAccepted = true;
	};

	$scope.cancelEditAccount = function(form) {
		initEditModel();

		$scope.form.$setPristine();
		$scope.form.$setUntouched();
		form.$dirty = false;
	}
})