myApp.controller('UploadController', function($scope, $http, $timeout){
	$scope.download_url = undefined;

	$scope.uploadObject = {
		file: undefined,
		passwordProtected: false,
		password: '',
		branch: undefined
	}

	$scope.upload = function(form) {
		var formData = new FormData();
		formData.append('file', $scope.uploadObject.file);
		formData.append('passwordProtected', $scope.uploadObject.passwordProtected);
		formData.append('password', $scope.uploadObject.password);
		formData.append('branch_id', $scope.uploadObject.branch.id);

		$http({
				headers: {'Content-Type': undefined},
	            method: 'POST',
	            url: 'api/upload',
	            data: formData
	        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            // open modal for copy link
            $scope.download_url = data.download_url;
            $('#share-link-modal').modal('show');
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	};

	$scope.triggerUpload = function () {
		var fileuploader = angular.element("#upload-input");

		fileuploader.on('click', function () {
		});

		$timeout(function(){
            angular.element(document.getElementById("upload-input")).trigger('click');
        })
	};

	$scope.cancelUploadForm = function(form){
		$scope.uploadObject = {
			file: undefined,
			passwordProtected: false,
			password: ''
		}

		angular.element('#upload-input').val('');

		form.$setPristine();
		form.$setUntouched();
	}

	$scope.setFile = function(file) {
		$scope.uploadObject.file = file;
	}

	$scope.createDragAndDropZone = function() {
		function handleFileSelect(evt) {
            evt.stopPropagation();
            evt.preventDefault();

            $scope.$apply($scope.setFile(evt.dataTransfer.files[0]));

            $('#upload-input').val('');
        }

        function handleDragOver(evt) {
            evt.stopPropagation();
            evt.preventDefault();
        }

        // Setup the dnd listeners.
        var dropZone = document.getElementById('dropZone');
        dropZone.addEventListener('dragover', handleDragOver, false);
        dropZone.addEventListener('drop', handleFileSelect, false);
	}

	$scope.copyToClipboardSuccess = function() {
        $scope.viewCopiedMessage = true;

        $timeout(function(){
            $scope.viewCopiedMessage = false;
        }, 3000);
    }

    $scope.addShareLinkModalEvent = function() {
    	$("#share-link-modal").on('hide.bs.modal', function () {
			$scope.$apply(function() {
				$scope.download_url = undefined;

	           	$scope.uploadObject = {
					file: undefined,
					passwordProtected: false,
					password: '',
					branch: undefined
				}
			})
    	});
    }
})