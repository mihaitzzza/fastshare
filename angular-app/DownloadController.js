myApp.controller('DownloadController', function($scope, $state, $stateParams, $http, $timeout, DownloadService) {
	$scope.file = undefined;
	$scope.finishedRequest = false;
	$scope.password = '';
	$scope.error = '';
	$scope.message = '';

	// load file from server
	var promise = DownloadService.getFile($stateParams.download_url);
	promise.then(function (data) {
		if(data.data.length > 0) {
			$scope.file = data.data[0];
		}

		$scope.finishedRequest = true;
	});

	var downloadFile = function() {
		var link = document.createElement("a");
		link.download = $scope.file.name;
		link.href = 'http://localhost/fastshare/' + $scope.file.path;
		link.click();
	}

	$(document).ready(function() {
		$("#request-password-modal").on('hide.bs.modal', function () {
           	$scope.password = '';

			$scope.$apply(); 
    	});
	})

	$scope.startDownload = function() {
		if($scope.file.password_protected) {
			$('#request-password-modal').modal('show');
		} else {
			downloadFile();
		}
	}

	$scope.confirmPassword = function() {
		$http({
            method: 'POST',
            url: 'api/confirm-download',
            data: {
            	id: $scope.file.id,
            	password: $scope.password
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            if(data.confirmed) {
            	$('#request-password-modal').modal('hide');

	            downloadFile();
            } else {
            	$scope.error = 'Parola gresita!'
            }

            $scope.password = '';
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}

	$scope.postMessage = function() {
		$http({
            method: 'POST',
            url: 'api/file-add-message',
            data: {
            	id: $scope.file.id,
            	message: $scope.message
            }
        })
        .success(function (data, status, headers, config) {
            console.log('SUCCESS FUNCTION', data);

            $scope.file = data.file;
            $scope.message = '';

            $scope.scrollChatToBottom();
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}

    $scope.getReadableFileSizeFormat = function() {
    	var response = '';

    	if($scope.file != undefined) {
    		return (Math.round(($scope.file.size / 1000000) * 100) / 100) + ' Mb';
    	}

    	return response;
    }

    $scope.scrollChatToBottom = function() {
        $("#file-chat").animate({ scrollTop: $('#file-chat').prop("scrollHeight")}, 500);
    }
})