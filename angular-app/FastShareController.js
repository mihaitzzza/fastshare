myApp.controller('FastShareController', function($rootScope, $scope, $auth, $http, $state, $stateParams, FastShareService) {
  $rootScope.errors = {};

  // load branches from server
  var promise = FastShareService.getBranches();
  promise.then(function (data) {
    $scope.branches = data.data;

    // emit event for childs for branch uploaded
    $scope.$broadcast('branchesLoaded', {});
  });

  $scope.logout = function() {
    $http({
      method: 'POST',
      url: 'api/logout'
    }).then(function successCallback(response) {
      $auth.logout();
      localStorage.removeItem('authUser');

      $auth.removeToken();

      $state.go('landing');
    }, function errorCallback(response) {

    });
  }

  $scope.isUserAuthenticated = function() {
    return $auth.isAuthenticated();
  }

  $scope.navigateToHomePage = function() {
    if($auth.isAuthenticated) {
      $state.go('files');
    } else {
      $state.go('landing');
    }
  }

  $scope.getDateFromString = function(stringDate) {
    return Date.parse(stringDate.replace('-', '/') + " UTC");
  }
})