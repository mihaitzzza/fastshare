myApp.controller('LoginController', function($scope, $http, $auth, $rootScope, $state, $location){
	$scope.credentials = {
		email: '',
		password: ''
	}

	$scope.login = function() {
		// $auth.login($scope.credentials).then(function() {
		// 	return $http.post('api/get-logged-user');
		// }, function(error) {
		// 	// Unauthorized (401)
		// 	if(error.status == 401) {
		// 		$rootScope.errors.login = 'Credenţialele introduse sunt incorecte!'
		// 	}
		// }).then(function(response) {
		// 	$rootScope.authUser = response.data.user;
		// 	localStorage.setItem('authUser', JSON.stringify(response.data.user));
			
		// 	// redirect to first page (/fisiere)
		// 	window.location.href = 'http://localhost/fastshare/fisiere';
		// });

		$auth.login($scope.credentials)
		.then(function(response) {
			$auth.setToken(response.data.token);

			// get logged user details
			$http({
	            method: 'POST',
	            url: 'api/get-logged-user'
	        })
	        .success(function (data, status, headers, config) {
	            console.log('SUCCESS FUNCTION', data);

            	$rootScope.authUser = data.user;
				localStorage.setItem('authUser', JSON.stringify(data.user));

				// redirect to first page (files)
				window.location.href = 'http://localhost/fastshare/fisiere';
	        })
	        .error(function (data, status, headers, config) {
	            console.log('ERROR FUNCTION', data);
	        });
		})
		.catch(function(response) {
			if(response.status == 401) {
				$scope.unauthorizedMessage = 'Credenţialele introduse sunt incorecte!'	
			}
		});
	}

	$(document).ready(function(){
		$("#login-modal").on('hide.bs.modal', function () {
           	$scope.credentials = {
				email: '',
				password: ''
			}
			
			$scope.unauthorizedMessage = undefined;

			$scope.loginForm.$setPristine();
			$scope.loginForm.$setUntouched();

			$scope.$apply(); 
    	});
	})
})