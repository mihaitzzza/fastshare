myApp.service("FastShareService", function ($http, $q) {
	this.getBranches = function() {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: 'api/get-branches'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {

		});

		return deferred.promise;
	}
});