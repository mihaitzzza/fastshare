myApp.service("DownloadService", function ($http, $q) {
	this.getFile = function(download_url) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: 'api/get-file/' + download_url
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {

		});

		return deferred.promise;
	}
});