myApp.service("FilesService", function ($http, $q) {
	this.getFiles = function() {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: 'api/get-files'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {

		});

		return deferred.promise;
	}
});