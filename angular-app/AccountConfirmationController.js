myApp.controller('AccountConfirmationController', function($scope, $http, $state, $auth, $rootScope) {
	$scope.data = {
		email: '',
		code: '',
		password: '',
		password_confirmation: ''
	}

	$scope.confirmAccount = function() {
		$http({
            method: 'POST',
            url: 'api/confirm',
            data: $scope.data
        })
        .success(function (data, status, headers, config) {
        	var credentials = {
        		email: $scope.data.email,
        		password: $scope.data.password
        	}

        	$auth.login(credentials).then(function() {
				return $http.post('api/get-logged-user');
			}, function(error) {
				window.alert('LoginController - ERROR!');
			}).then(function(response) {
				$rootScope.authUser = response.data.user;
				localStorage.setItem('authUser', JSON.stringify(response.data.user));
				
				$state.go('files');
			});
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);
        });
	}
})