myApp.controller('RegisterController', function($scope, $http, $timeout){
	$scope.registerData = {
		firstname:'',
		lastname:'',
		email: ''
	}

	$scope.register = function() {
		$http({
            method: 'POST',
            url: 'api/register',
            data: $scope.registerData
        })
        .success(function (data, status, headers, config) {
        	window.location.href = 'http://localhost/fastshare/inregistrare';
        })
        .error(function (data, status, headers, config) {
            console.log('ERROR FUNCTION', data);

            if(status == 422) {
            	// E-mail address already exists
            	$scope.uniqueEmailMessage = 'Adresa de e-mail este deja înregistrată!';
            }
        });
	}

	$(document).ready(function(){
		$("#register-modal").on('hide.bs.modal', function () {
           	$scope.registerData = {
				firstname:'',
				lastname:'',
				email: ''
			}

			$scope.uniqueEmailMessage = undefined;

			$scope.registerForm.$setPristine();
			$scope.registerForm.$setUntouched(); 

			$scope.$apply();
    	});
	})
})