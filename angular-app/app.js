var myApp = angular.module('FastShare', [
    'ui.router',
    'ngMessages',
    'satellizer',
    'ngAnimate',
    'ngTouch',
    'ngMaterial',
    'ngDomEvents',
    'ui.bootstrap',
    'angular-clipboard',
    'ngImgCrop'
]);

myApp.config(function($stateProvider, $urlRouterProvider, $locationProvider, $authProvider) {

    $authProvider.loginUrl = 'fastshare/api/login';

    $urlRouterProvider.otherwise("/404");
    //
    // Now set up the states
    $stateProvider
        .state('landing', {
            url: "/",
            templateUrl: "views/landing.html",
            data: {
                requireLogin: false
            }
        })
        .state('404', {
            url: "/404",
            templateUrl: "views/404.html",
            data: {
                notFound: true
            }
        })
        .state('confirm', {
            url: "/confirmare",
            templateUrl: "views/confirm.html",
            data: {
                requireLogin: false
            }
        })
        .state('files', {
            url: "/fisiere",
            templateUrl: "views/files.html",
            data: {
                requireLogin: true
            }
        })
        .state('upload', {
            url: "/incarca_fisiere",
            templateUrl: "views/upload.html",
            data: {
                requireLogin: true
            }
        })
        .state('forum', {
            url: "/forum",
            templateUrl: "views/forum.html",
            data: {
                requireLogin: true
            }
        })
        .state('forum.subjects', {
            url: "/{forum_name}/subiecte",
            data: {
                requireLogin: true
            }
        })
        .state('forum.subjects.messages', {
            url: "/{subject_id}",
            data: {
                requireLogin: true
            }
        })
        .state('settings', {
            url: "/setari",
            templateUrl: "views/settings.html",
            data: {
                requireLogin: true
            }
        })
        .state('register', {
            url: "/inregistrare",
            templateUrl: "views/register-succeed.html",
            data: {
                requireLogin: false
            }
        })
        .state('download', {
            url: "/download/{download_url}",
            templateUrl: 'views/download.html'
        })

    $locationProvider.html5Mode(true);
});

myApp.run(function($rootScope, $state, $auth, $uibModalStack) {
    $uibModalStack.dismissAll();

    $rootScope.state = $state;

    $rootScope.$on('$stateChangeStart', function(event, toState) {
        var stateRequireLogin = undefined;
        var stateNotFound = false;

        if (toState.data != undefined) {
            stateRequireLogin = toState.data.requireLogin;
        }

        if (toState.data != undefined && toState.data.requireLogin) {
            stateRequireLogin = true;
        }

        if (toState.data != undefined && toState.data.notFound) {
            stateNotFound = true;
        }

        if (!stateNotFound) {
            if (stateRequireLogin != undefined) {
                if ($auth.isAuthenticated() && !stateRequireLogin) {
                    event.preventDefault();

                    // get authenticated user from local storage
                    $rootScope.authUser = JSON.parse(localStorage.getItem('authUser'));

                    $state.go('files');
                }

                if (!$auth.isAuthenticated() && stateRequireLogin) {
                    event.preventDefault();
                    $state.go('landing');
                }
            }
        }

        if ($auth.isAuthenticated()) {
            // get authenticated user from local storage
            $rootScope.authUser = JSON.parse(localStorage.getItem('authUser'));
        }
    });
});

myApp.directive("ngFileSelect", function() {
    return {
        link: function($scope, el) {
            el.bind("change", function(e) {
                $scope.$apply($scope.setFile(e.target.files[0]));
            });
        }
    }
});

myApp.directive('notification', function($timeout) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="alert alert-{{ notification.type }}" style="width:50%; margin:0 auto;" ng-show="notification.message" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ notification.message }}</div>',
    };
});

myApp.directive('validateConfirmation', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, $element, $attrs, $ctrl) {
            var validate = function (modelValue) {
                var receivedObject = $attrs.validateConfirmation;

                if(receivedObject === modelValue) {
                    $ctrl.$setValidity('confirmed', true);
                } else {
                    $ctrl.$setValidity('confirmed', false);
                }

                return modelValue;
            };

            $ctrl.$parsers.unshift(validate);
            $ctrl.$formatters.push(validate);

            $attrs.$observe('validateConfirmation', function (comparisonModel) {
                return validate($ctrl.$viewValue);
            });
        }
    }
})

myApp.filter('trim', function() {
    return function(files, tab, user_id, branch_id) {
        var newList = [];

        if (tab == 1) {
            angular.forEach(files, function(file) {
                if(file.branch_id == branch_id) {
                    newList.push(file);
                }
            })
        }
        else {
            angular.forEach(files, function(file) {
                if(file.user_id == user_id) {
                    newList.push(file);
                }
            })
        }

        return newList;
    }
})