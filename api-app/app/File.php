<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $hidden = [
        'password',
    ];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function branch() {
        return $this->belongsTo('App\Branch', 'branch_id');
    }

    public function messages() {
    	return $this->belongsToMany(
            'App\Message',
            'message_on_file',
            'file_id',
            'message_id'
        )->withTimestamps();
    }
}
