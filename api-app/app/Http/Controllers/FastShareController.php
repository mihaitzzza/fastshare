<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use Mail;
use Validator;
use Carbon\Carbon;
use Hash;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;
use App\File;
use App\Message;
use App\Branch;
use App\Subject;

class FastShareController extends Controller
{
    public function getBranches() {
        return Branch::all()->load('subjects.user', 'subjects.messages.user');
    }

	public function upload(Request $request) {
		Log::info($request->all());

		if(! $request->hasFile('file')) {
			return response()->json([], 400);	
		}

		// upload file
		try {
			$download_url = DB::transaction(function() use($request) {
                $authUser = null;

                if(JWTAuth::getToken()) {
                    $authUser = JWTAuth::parseToken()->toUser();
                }

                $file = new File();

                if($authUser != null) {
                    $file->user()->associate($authUser);
                    $file->expire_date = (new Carbon())->addDays(30);
                } else {
                    $file->expire_date = (new Carbon())->addDays(3);
                }

                $file->branch()->associate(Branch::findOrFail($request->input('branch_id')));
                $file->name = $request->file('file')->getClientOriginalName();
                $file->size = $request->file('file')->getSize();
                $file->download_url = str_random(50);
                $file->path = $this->uploadFile($request->file('file'));

                if($request->input('password')) {
                    $file->password = bcrypt($request->input('password'));
                    $file->password_protected = 1;
                }

                $file->save();

                if($authUser != null) {
                    $mailParameters = [
                        'user' => $authUser,
                        'fileShareURL' => url('download/'.$file->download_url),
                        'fileName' => $file->name,
                        'password' => $request->input('password')
                    ];

                    Mail::send('emails.new-upload', $mailParameters, function($message) use ($authUser){
                        $message->from('no-reply@fastshare.ro', "FastShare");
                        $message->subject("FastShare - Incarcare noua");
                        $message->to($authUser->email);
                    });
                }

                return $file->download_url;
            });

            return response()->json([
                'download_url' => $download_url
            ], 200);
		} catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
		}
	}

    public function download($download_url) {
        $files = File::where('download_url', '=', $download_url)->get();

        if(! $files->isEmpty()) {
            if(Carbon::now()->gt(Carbon::parse($files[0]->expire_date))) {
                return [];
            }
        }

        return $files->load('user', 'messages.user', 'branch');
    }

    public function getFiles(Request $request) {
        $files = File::all();

        return response()->json([
            'files' => $files
        ], 200);
    }

    public function deleteFile(Request $request) {
        Log::info($request->all());

        try {
            DB::transaction(function() use($request) {
                $file = File::findOrFail($request->input('id'));
                $file->delete();
            });

            return response()->json([], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function confirmDownload(Request $request) {
        Log::info($request->all());

        try {
            $confirmed = DB::transaction(function() use($request) {
                $file = File::findOrFail($request->input('id'));
                
                if(Hash::check($request->input('password'), $file->password)) {
                    return true;
                } else {
                    return false;
                }
            });

            return response()->json([
                'confirmed' => $confirmed
            ], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function fileAddMessage(Request $request) {
        Log::info($request->all());

        try {
            $file = DB::transaction(function() use($request) {
                $authUser = JWTAuth::parseToken()->toUser();
                $file = File::findOrFail($request->input('id'));

                $message = new Message();
                $message->user()->associate($authUser);
                $message->text = $request->input('message');
                $message->save();

                $file->messages()->attach($message);

                return $file->load('user', 'messages.user', 'branch');
            });

            return response()->json([
                'file' => $file
            ], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function addNewSubject(Request $request) {
        Log::info($request->all());
        
        try {
            $subject = DB::transaction(function() use($request) {
                $authUser = JWTAuth::parseToken()->toUser();
                $branch = Branch::findOrFail($request->input('branch_id'));

                $subject = new Subject();
                $subject->user()->associate($authUser);
                $subject->branch()->associate($branch);
                $subject->title = $request->input('new_subject')['title'];
                $subject->description = $request->input('new_subject')['description'];
                $subject->save();

                return $subject->load('user', 'messages.user');
            });

            return response()->json([
                'subject' => $subject
            ], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function subjectAddMessage(Request $request) {
        Log::info($request->all());

        try {
            $message = DB::transaction(function() use($request) {
                $authUser = JWTAuth::parseToken()->toUser();
                $subject = Subject::findOrFail($request->input('subject_id'));

                $message = new Message();
                $message->user()->associate($authUser);
                $message->text = $request->input('new_message')['text'];
                $message->save();

                $subject->messages()->attach($message);

                return $message->load('user');
            });

            return response()->json([
                'message' => $message
            ], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function deleteSubject(Request $request) {
        Log::info($request->all());

        try {
            DB::transaction(function() use($request) {
                $subject = Subject::findOrFail($request->input('subject_id'));
                $subject->delete();
            });

            return response()->json([], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function deleteMessage(Request $request) {
        Log::info($request->all());

        try {
            DB::transaction(function() use($request) {
                $message = Message::findOrFail($request->input('message_id'));
                $message->delete();
            });

            return response()->json([], 200);
        } catch(\Exception $e) {
            Log::info($e);
            return response()->json([], 422);
        }
    }

    public function saveSettings(Request $request) {
        Log::info($request->all());

        $authUser = JWTAuth::parseToken()->toUser();

        $validationRules = [
            'firstname' => "required|regex:/^[a-zA-Z](['\s-]?[a-zA-Z])*$/",
            'lastname' => "required|regex:/^[a-zA-Z](['\s-]?[a-zA-Z])*$/",
            'email' => "required|regex:/^[a-zA-Z]([._]?[a-zA-Z0-9])*[@][a-zA-Z]+(\.[a-zA-Z]+)+$/",
            'new_password' => "confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/"
        ];

        if($request->input('email') != $authUser->email) {
            $validationRules['email'] = "required|unique:users|regex:/^[a-zA-Z]([._]?[a-zA-Z0-9])*[@][a-zA-Z]+(\.[a-zA-Z]+)+$/";
        }

        $validator = Validator::make($request->all(), $validationRules);

        if($request->input('new_password')) {
            if(!Hash::check($request->input('old_password'), $authUser->password)) {
                $validator->errors()->add('old_password', 'Old password doesn\'t match.');
            }
        }

        if (! $validator->errors()->isEmpty()) {
            return response()->json(array(
                'errors' => $validator->getMessageBag()
            ), 400);
        }

        try {
            $user = DB::transaction(function() use($request) {
                $user = User::findOrFail($request->input('id'));

                if($user->firstname !== $request->input('firstname')) {
                    $user->firstname = $request->input('firstname');
                }

                if($user->lastname !== $request->input('lastname')) {
                    $user->lastname = $request->input('lastname');
                }

                if($user->email !== $request->input('email')) {
                    $user->email = $request->input('email');
                }

                if($request->input('new_password')) {
                    $user->password = bcrypt($request->input('new_password'));
                }

                if($request->hasFile('file')) {
                    $user->avatar = $this->uploadFile($request->file('file'));
                }

                $user->save();

                return $user;
            });

            return response()->json([
                'authUser' => $user
            ], 200);
        } catch(\Exception $e) {
            Log::info($e);

            return response()->json([
                'success' => true
            ], 400);
        }

        return response()->json(['success' => true], 200);
    }

	private function uploadFile(UploadedFile $file) {
        $user = null;

        if(JWTAuth::getToken()) {
            $user = JWTAuth::parseToken()->toUser();
        }

        $destinationPath = 'files';

        $extension = $file->getClientOriginalExtension();

        $fileName = '';
        if($user != null) {
            $fileName = uniqid() . str_random(20) . '.' . $extension;
        } else {
            $fileName = str_random(10) . str_random(20) . '.' . $extension;
        }

        $attachmentUrl = $destinationPath .'/'. $fileName;

        $upload_success = $file->move($destinationPath, $fileName);

        if($upload_success) {
            return $attachmentUrl;
        }
    }
}
