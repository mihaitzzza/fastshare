<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use Mail;
use Validator;

use Illuminate\Support\Facades\DB;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;

class AuthenticateController extends Controller
{
    public function getUserLogged(Request $request) {
		$authUser = JWTAuth::parseToken()->toUser();

        Log::info('authUser');
        Log::info($authUser);

		return response()->json([
			'success' => true,
			'user' => $authUser
		], 200);
	}

    public function login(Request $request) {
    	Log::info($request->all());

    	$credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function logout(){
        JWTAuth::invalidate(JWTAuth::getToken());
    }

    public function register(Request $request) {
    	Log::info($request->all());

    	// validate data
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
        ]);

        if($validator->fails()) {
        	return response()->json([
        		'errors' => $validator->getMessageBag()
        	], 422);
        }

        // save user in database
        try {
        	DB::transaction(function() use($request) {
                $user = new User();
		    	$user->firstname = $request->input('firstname');
		    	$user->lastname = $request->input('lastname');
		    	$user->email = $request->input('email');

		    	$code = str_random(20);
		        $user->confirmation_code = $code;
		    	
		        $user->save();

		    	$mailParameters = [
		    		'user' => $user,
		    		'code' => $code
		    	];

		    	Mail::send('emails.new-user', $mailParameters, function($message) use ($user){
		            $message->from('no-reply@fastshare.ro', "FastShare");
		            $message->subject("FastShare - Confirmare cont");
		            $message->to($user->email);
		        });
            });

        	// return success (200)
            return response()->json([], 200);
        } catch(\Exception $e) {
        	// return BAD REQUEST (400)
        	return response()->json([], 400);
        }
    }

    public function confirmAccount(Request $request) {
        Log::info('confirmAccount');
        Log::info($request->all());

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8'
        ]);

        $users = User::where('email', '=', $request->input('email'))->get();
        
        if($users->isEmpty()) {
            $validator->errors()->add('email', 'Nu exista cont pentru aceasta adresa!');
        } else {
            if($users[0]->password != null) {
                $validator->errors()->add('confirmed', 'Contul a fost deja confirmat!');
            }

            if($users[0]->confirmation_code != $request->input('code')) {
                $validator->errors()->add('code', 'Codul de confirmare este invalid!');
            }
        }

        if(! $validator->getMessageBag()->isEmpty()) {
            return response()->json(array(
                'errors' => $validator->getMessageBag()
            ), 422);
        }

        try {
        	DB::transaction(function() use($request, $users) {
                $user = $users[0];
		        $user->password = bcrypt($request->input('password'));
		        $user->save();
            });

        	// return success (200)
            return response()->json([], 200);
        } catch(\Exception $e) {
        	// return BAD REQUEST (400)
        	return response()->json([], 400);
        }
    }
}
