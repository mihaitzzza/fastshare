<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authenticate controller
Route::post('/api/login', 'AuthenticateController@login');
Route::post('/api/register', 'AuthenticateController@register');
Route::post('/api/get-logged-user', 'AuthenticateController@getUserLogged');
Route::post('/api/confirm', 'AuthenticateController@confirmAccount');
Route::post('/api/logout', 'AuthenticateController@logout');

// Application controller
Route::post('api/get-branches', 'FastShareController@getBranches');
Route::post('/api/upload', 'FastShareController@upload');
Route::get('/api/get-file/{download_url}', 'FastShareController@download');
Route::get('/api/get-files', 'FastShareController@getFiles');
Route::post('/api/delete-file', 'FastShareController@deleteFile');
Route::post('/api/confirm-download', 'FastShareController@confirmDownload');
Route::post('/api/file-add-message', 'FastShareController@fileAddMessage');
Route::post('/api/add-subject', 'FastShareController@addNewSubject');
Route::post('/api/delete-subject', 'FastShareController@deleteSubject');
Route::post('/api/subject-add-message', 'FastShareController@subjectAddMessage');
Route::post('/api/delete-message', 'FastShareController@deleteMessage');
Route::post('/api/save-settings', 'FastShareController@saveSettings');