<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function branch() {
    	return $this->belongsTo('App\Branch', 'branch_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function messages() {
    	return $this->belongsToMany(
            'App\Message',
            'message_on_subject',
            'subject_id',
            'message_id'
        )->withTimestamps();
    }
}
