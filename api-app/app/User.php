<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $hidden = [
        'password',
    ];

    public function files() {
        return $this->belongsTo('App\File', 'user_id');
    }
}
