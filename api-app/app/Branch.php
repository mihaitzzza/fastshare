<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function files() {
        return $this->hasMany('App\File', 'branch_id');
    }

    public function subjects() {
    	return $this->hasMany('App\Subject', 'branch_id');
    }
}
