<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('branches')->insert([
			'id' => 1,
			'name' => 'Agricultură'
		]);

		DB::table('branches')->insert([
			'id' => 2,
			'name' => 'Alchimie'
		]);

		DB::table('branches')->insert([
			'id' => 3,
			'name' => 'Arheologie'
		]);


		DB::table('branches')->insert([
			'id' => 4,
			'name' => 'Arhitectură'
		]);

		DB::table('branches')->insert([
			'id' => 5,
			'name' => 'Arte'
		]);

		DB::table('branches')->insert([
			'id' => 6,
			'name' => 'Astronomie'
		]);

		DB::table('branches')->insert([
			'id' => 7,
			'name' => 'Alchimie'
		]);

		DB::table('branches')->insert([
			'id' => 8,
			'name' => 'Biologie'
		]);

		DB::table('branches')->insert([
			'id' => 9,
			'name' => 'Drept'
		]);

		DB::table('branches')->insert([
			'id' => 10,
			'name' => 'Economie'
		]);

		DB::table('branches')->insert([
			'id' => 11,
			'name' => 'Educație'
		]);

		DB::table('branches')->insert([
			'id' => 12,
			'name' => 'Geologie'
		]);


		DB::table('branches')->insert([
			'id' => 13,
			'name' => 'Imobiliare'
		]);

		DB::table('branches')->insert([
			'id' => 14,
			'name' => 'IT'
		]);

		DB::table('branches')->insert([
			'id' => 15,
			'name' => 'Literatură'
		]);

		DB::table('branches')->insert([
			'id' => 16,
			'name' => 'Matematică'
		]);

		DB::table('branches')->insert([
			'id' => 17,
			'name' => 'Modă'
		]);

		DB::table('branches')->insert([
			'id' => 18,
			'name' => 'Muzică'
		]);

		DB::table('branches')->insert([
			'id' => 19,
			'name' => 'Sănătate'
		]);

		DB::table('branches')->insert([
			'id' => 20,
			'name' => 'Sport'
		]);

		DB::table('branches')->insert([
			'id' => 21,
			'name' => 'Turism'
		]);

		DB::table('branches')->insert([
			'id' => 22,
			'name' => 'Altele'
		]);
		
	}
}
