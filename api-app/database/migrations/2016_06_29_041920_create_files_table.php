<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('size');
            $table->timestamp('expire_date');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('branch_id');
            $table->string('download_url');
            $table->string('path');
            $table->string('password')->nullable();
            $table->tinyInteger('password_protected')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
