<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->text('text');
            $table->timestamps();
        });

        Schema::create('message_on_subject', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('message_id');
            $table->timestamps();
        });

        Schema::create('message_on_file', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('message_id');
            $table->unsignedInteger('file_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
