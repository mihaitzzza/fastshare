<!DOCTYPE html>
<html style="background-color:white;">
    <head>
        <base href="/fastshare/">

        <title>FastShare</title>

        <link href="{{url('/css/lib/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
        <link href="{{url('/css/lib/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
        <link href="{{url('/css/css.css')}}" type="text/css" rel="stylesheet">
        <link href="{{url('/css/chat.css')}}" type="text/css" rel="stylesheet">

        <script src="{{url('/js/lib/jquery.min.js')}}"></script>
        <script src="{{url('/js/lib/bootstrap.min.js')}}"></script>

        <script src="{{url('/js/lib/angular.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-animate.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-touch.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-ui-router.js')}}"></script>
        <script src="{{url('/js/lib/angular-messages.min.js')}}"></script>
        <script src="{{url('/js/lib/satellizer.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-dom-events.js')}}"></script>
        <script src="{{url('/js/lib/angular-aria.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-material.min.js')}}"></script>
        <script src="{{url('/js/lib/angular-clipboard.js')}}"></script>
        <script src="{{url('/js/lib/ui-bootstrap-tpls-1.3.3.min.js')}}"></script>
        <script src="{{url('/js/lib/ng-img-crop.js')}}"></script>

        <script src="{{url('/angular-app/app.js')}}"></script>
        <script src="{{url('/angular-app/FastShareController.js')}}"></script>
        <script src="{{url('/angular-app/FastShareService.js')}}"></script>
        <script src="{{url('/angular-app/RedirectController.js')}}"></script>
        <script src="{{url('/angular-app/LoginController.js')}}"></script>
        <script src="{{url('/angular-app/RegisterController.js')}}"></script>
        <script src="{{url('/angular-app/AccountConfirmationController.js')}}"></script>
        <script src="{{url('/angular-app/FilesController.js')}}"></script>
        <script src="{{url('/angular-app/FilesService.js')}}"></script>
        <script src="{{url('/angular-app/UploadController.js')}}"></script>
        <script src="{{url('/angular-app/DownloadController.js')}}"></script>
        <script src="{{url('/angular-app/DownloadService.js')}}"></script>
        <script src="{{url('/angular-app/ForumController.js')}}"></script>
        <script src="{{url('/angular-app/SettingsController.js')}}"></script>

        <script src="{{url('/js/lib/upload.js')}}"></script>
    </head>
    <body ng-app="FastShare" ng-controller="FastShareController">
        <div ui-view></div>
    </body>
</html>
