Buna ziua, {{$user->lastname}} {{$user->firstname}},
<br/><br/>
Fisierul <strong>{{$fileName}}</strong> poate fi distribuit prin link-ul:<br/><strong>{{$fileShareURL}}</strong>
<br/><br/>
@if($password)
	<strong>Parola:</strong> {{$password}}
	<br/><br/>
@endif
Va multumim!