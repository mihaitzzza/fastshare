Buna ziua, {{$user->lastname}} {{$user->firstname}},
<br/><br/>
A fost creat un nou cont pe aplicatia <strong>FastShare</strong> pentru adresa de e-mail <strong>{{$user->email}}</strong>.
<br/><br/>
<strong>Cod de confirmare:</strong> {{$code}}
<br/><br/>
Pentru a confirma acest cont, va rugam sa faceti click <a href="{{url('/confirmare')}}">aici</a>!<br/><br/>
Va multumim!